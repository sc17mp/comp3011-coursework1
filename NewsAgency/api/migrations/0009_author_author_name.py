# Generated by Django 3.1.7 on 2021-03-21 13:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_auto_20210321_0014'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='author_name',
            field=models.CharField(default='', max_length=1000, null=True),
        ),
    ]
