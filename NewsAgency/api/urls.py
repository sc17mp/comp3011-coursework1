from django.urls import path

from . import views

urlpatterns = [
    path('api', views.index),
    path('api/login/', views.handle_login),
    path('api/logout/', views.handle_logout),
    path( 'api/poststory/' , views.post_story),
    path( 'api/getstories/' , views.get_stories),
    path( 'api/deletestory/' , views.delete_story),
]