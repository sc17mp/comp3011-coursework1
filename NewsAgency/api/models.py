from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

category_list = [('pol','pol'), ('art','art'),('tech','tech'), ('trivia','trivia')]
region_list = [('uk','uk'),('eu','eu'),('w','w')]

class Author(models.Model):
    author_name = models.CharField(max_length=1000, default="")
    user = models.OneToOneField(User, on_delete=models.CASCADE, default="")

    def get_name( self ):
        return self.author_name

class NewsStory(models.Model):
    key = models.AutoField(primary_key=True, unique=True)
    headline = models.CharField(max_length=64)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, db_column='author')
    story_cat = models.CharField(max_length=6, choices=category_list, default='')
    story_region = models.CharField(max_length=2, choices=region_list, default='')
    story_details = models.CharField(max_length=512)
    story_date = models.DateField(default=timezone.now)