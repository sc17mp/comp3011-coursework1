import datetime
import json
import traceback
from dateutil.utils import today
from django.contrib.auth import authenticate
from django.http import HttpResponse , JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

from . import models

# Create your views here.
def index(request):
    return HttpResponse('Hello world')

@csrf_exempt
def handle_login(request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username,password=password)
        if user is not None:
            request.session['user'] = username
            # print("Session:",request.session['user'])
            return HttpResponse ( status=200 )
        else:
            # traceback.print_exc ()
            return HttpResponse ( status=404,content="User not found" )

@csrf_exempt
def handle_logout(request):
    try:
        del request.session['user']
        return HttpResponse(status=200, content="Logged out successfully")
    except KeyError:
        return HttpResponse(status=404, content="Error: User not logged in")

@csrf_exempt
def post_story(request):
    req = json.loads(request.body)
    headlines = req['headline']
    category = req['category']
    region = req['region']
    story_details = req['details']
    current_auth = get_current_user(request)
    if current_auth is None:
        return HttpResponse(status=503, content="Service unavailable: user not logged in")
    try:
        new_story = models.NewsStory.objects.create(headline=headlines,
                                                    author=current_auth,
                                                    story_cat=category, story_region=region,
                                                    story_details = story_details, story_date=today())
        new_story.save()
        return HttpResponse(status=201, content="Story create successful")
    except:
        # traceback.print_exc ()
        return HttpResponse(status=503,content="Service unavailable")

def get_stories(request):
    req = json.loads(request.body)
    category = req['story_cat']
    region = req['story_region']
    date = req['story_date']
    if date != "*":
        date_split = date.split ( '/' )
        # print(date_split)
        day = int ( date_split[0] )
        month = int ( date_split[1] )
        year = int ( date_split[2] )
        # print("Date query: ",datetime.date(year, month, day))
    try:
        if category == "*" and region == "*" and date == "*":
            print ("All stories", models.NewsStory.objects.all() )
            stories = models.NewsStory.objects.all()
        else:
            if category != "*":
                if region != "*":
                    if date != "*":
                        stories = models.NewsStory.objects.all ().filter ( story_cat=category , story_region=region ,
                                                                           story_date=datetime.date(year, month, day))
                    else:
                        stories = models.NewsStory.objects.all().filter (story_cat=category, story_region=region)
                else:
                    stories = models.NewsStory.objects.all ().filter ( story_cat=category)
            else:
                if region != "*":
                    if date != "*":
                        stories = models.NewsStory.objects.all ().filter ( story_region=region,
                                                                           story_date=datetime.date(year, month, day))
                    else:
                        stories = models.NewsStory.objects.all ().filter ( story_region=region )
                else:
                    if date != "*":
                        stories = models.NewsStory.objects.all ().filter ( story_date=datetime.date(year, month, day))
        stories_list = list()
        for story in list(stories.values()):
            print(story)
            author_obj = User.objects.get(id=story['author_id'])
            print(author_obj)
            author_name = models.Author.objects.get(user=author_obj).get_name()
            print("Author name:", author_name)
            story_item = {
                'key': story['key'],
                'headline': story['headline'],
                'story_cat': story['story_cat'],
                'story_region': story['story_region'],
                'story_date': story['story_date'],
                'story_details': story['story_details'],
                'author': author_name,
            }
            stories_list.append(story_item)
        if len(stories_list) == 0:
            return HttpResponse(status=404,content="No story found")
        else:
            return JsonResponse({'stories': stories_list}, status=200, safe=False)
    except:
        # traceback.print_exc ()
        return HttpResponse(status=404,content="No story found")

@csrf_exempt
def delete_story(request):
    req = json.loads(request.body)
    story_key = req['story_key']
    current_auth = get_current_user(request)
    # print("current user:", current_auth)
    if current_auth is None:
        return HttpResponse(status=503,content="Service unavailable: user not logged in")
    try:
        story = models.NewsStory.objects.get(key=story_key)
        story.delete()
        return HttpResponse(status=201, content="Delete story {}".format(story_key))
    except:
        traceback.print_exc ()
        return HttpResponse(status=503,content="Service unavailable. Story with key {} may not exist".format(story_key))

def get_current_user(request):
    try:
        current_user = User.objects.get(username=request.session['user'])
        current_auth = models.Author.objects.get(user=current_user)
        return current_auth
    except KeyError:
        return None

